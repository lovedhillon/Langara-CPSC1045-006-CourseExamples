//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("I am working!!");
function sliderXlistener(){
    let sliderX = document.querySelector("#rangeX");
    let circle = document.querySelector("#circle");
    circle.setAttribute("cx", sliderX.value);
    console.log(circle); //for debugging, so we can see the circle in the console
}

function sliderYlistener(){
    let sliderY = document.querySelector("#rangeY");
    let circle = document.querySelector("#circle");
    circle.setAttribute("cy", sliderY.value);
    console.log(circle); //for debugging, so we can see the circle in the console  
}
function changeColor(){
   let colorPicker = document.querySelector("#color");
   let circle = document.querySelector("#circle");
   circle.setAttribute("fill", colorPicker.value);
}

//exporting the function to the global namespace
//Should be a better way, but doing this for now
window.sliderXlistener = sliderXlistener;
window.sliderYlistener = sliderYlistener;
window.changeColor = changeColor;